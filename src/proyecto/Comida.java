package proyecto;

/**
 * @author leencarnacion
 * @author cavargas10
 */
public abstract class Comida { // clase abstracta

    public String platoInicial;
    public double platoPrincipal;
    public double cant;
    public double total;

    public double getTotal() { // Devuelve el total
        return total;
    } // Fin del metodo getTotal

    public abstract double carcularValor(); // metodo abstracto

    // Devuelve representacion en String 
    @Override
    public String toString() {
        return "Cantidad: \tDetalle:\tc/u:\n"
                + this.cant +"\t\t"+ this.platoInicial+"\t" +this.platoPrincipal+"$\n";
    }

}

class Desayuno extends Comida { //clase

    public double postre;

    // Constructor con tres agrumentos
    public Desayuno(String platoInicial, double platoPrincipal, double cant) {
        this.platoInicial = platoInicial;
        this.platoPrincipal = platoPrincipal;
        this.cant = cant;
        this.postre = 0.75;
        carcularValor();
    } // Fin del Constructor Dasayuno con tres argumentos

    @Override
    public double carcularValor() {
        this.total = (this.platoPrincipal + this.postre) * (this.cant);
        return this.total;
    }

    // Devuelve representacion en String
    @Override
    public String toString() {
        return super.toString() + "\t\tPOSTRE\t\t" + this.postre + "$\nSubTotal: " + this.total + " $\n";
    }

}

class Almuerzo extends Comida { //clase

    String platosegundo;
    public double segundo;

    // Constructor con cinco argumentos
    public Almuerzo(String platoInicial, double platoPrincipal, double cant, double segundo, String platosegundo) { //Constructor
        this.platoInicial = platoInicial;
        this.platoPrincipal = platoPrincipal;
        this.cant = cant;
        this.segundo = segundo;
        this.platosegundo = platosegundo;
        carcularValor();
    }// Fin constructor

    @Override
    public double carcularValor() {
        this.total = (this.platoPrincipal + this.segundo) * this.cant;
        return this.total;
    }

    // Devuelve representacion en String
    @Override
    public String toString() {
        return super.toString() + "\t\t"+this.platosegundo + "\t" + this.segundo + "$\nSubtotal:" + this.total + " $\n";
    }

}

class Merienda extends Comida { // clase
    // Constructor con tres argumentos

    public Merienda(String platoInicial, double platoPrincipal, double cant) { //Constructor
        this.platoInicial = platoInicial;
        this.platoPrincipal = platoPrincipal;
        this.cant = cant;
        carcularValor(); //
    } //Fin constructor

    @Override
    public double carcularValor() {
        this.total = (this.platoPrincipal) * this.cant;
        return this.total;
    }

    // Devuelve representacion en String
    @Override
    public String toString() {
        return super.toString() + "subTotal: " + this.total + "$\n";
    }
}
