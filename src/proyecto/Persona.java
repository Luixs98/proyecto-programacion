package proyecto;

/**
 * @author leencarnacion
 * @author cavargas10
 */
public class Persona {//Clase

    public String nombres;

    public String apellidos;

    private int id;

    private String tipo;

    public Persona() {//Constructor
    }

    public Persona(String nombres, String apellidos, int id) {//Constructor
        setNombres(nombres);
        setApellidos(apellidos);
        setId(id);
    }

    // Metodo para setNombres
    public void setNombres(String nombres) {
        this.nombres = nombres; // Deberia validar
    } // Fin metodo para setNombres

    // Metodo para setApellidos
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos; // Deberia validar
    } //Fin metodo para setApellidos

    // Metodo para setId
    public void setId(int id) {
        this.id = id; // Deberia validar
    } // Fin metodo setId

    // Metodo para setTipo
    public void setTipo(String tipo) {
        this.tipo = tipo; // Deberia validar
    } // Fin metodo para setTipo

    // Metodo para devolver el Id
    public int getId() {
        return id;
    } // Fin metodo para devolver Id

    @Override
    public String toString() {//metodo
        return "\nNOMBRE: " + this.nombres + "\nAPELLIDOS: " + this.apellidos + "\nId: " + this.id + "\nTIPO: " + this.tipo + "\n";
    }
}

class Cliente extends Persona {//Clase

    private int telefono;

    private String direccion;

    public int gastos;
    
    public Cliente() {//constructor
    }

    public Cliente(int telefono, String direccion, String nombres, String apellidos, int id) {//constructor
        super(nombres, apellidos, id);
        this.telefono = telefono;
        this.direccion = direccion;
        setTipo("Cliente");
    }

    @Override
    public String toString() {//metodo
        return super.toString() + "TELEFONO: " + this.telefono + "\nDIRECCION: " + this.direccion;
    }

}

class Empleado extends Persona {//Clase

    private double horasTrabajadas;

    private double bono;

    private double sueldo;

    private double costoPorHora;

    public Empleado() {//Constructor
    }

    // Constructor de 6 argumentos
    public Empleado(double horasTrabajadas, double bono, double costoPorHora,
            String nombres, String apellidos, int id) {
        super(nombres, apellidos, id);
        this.horasTrabajadas = horasTrabajadas;
        this.bono = bono;
        this.costoPorHora = costoPorHora;
        calcularSueldo();
    }

    // Metodo calcularSueldo 
    public void calcularSueldo() {//metodo
        this.sueldo = ((this.costoPorHora * this.horasTrabajadas) + this.bono);
    } // Fin metodo

    // Metodo para setSueldo
    public void setSueldo(double sueldo) {
        this.sueldo = sueldo; //Deberia validar
    } // Fin metodo setSueldo

    public double getSueldo() {//metodo
        return this.sueldo;
    }

    // Metodo para obtener CostoPorHoras
    public double getCostoPorHoras() {
        return this.costoPorHora;
    } // Fin de metodo

    // Metodo para obtener HorasTrabajadas
    public double getHorasTrabajadas() {
        return this.horasTrabajadas;
    } // Fin de metodo

    // MEtodo para obtener Bono
    public double getBono() {
        return this.bono;
    }// Fin de metodo

    @Override
    public String toString() {//metodo
        return super.toString() + "VALOR A PAGAR: " + getSueldo() + "\n";
    }

}

class Supervisor extends Empleado {//Clase

    public double bono2 = 0; // Inicializar bono2

    public Supervisor() { //Constructor vacio
    }

    //Constructor con siete argumentos
    public Supervisor(double horasTrabajadas, double bono, double costoPorHora, String nombres, String apellidos, int id, double bono2) {
        super(horasTrabajadas, bono, costoPorHora, nombres, apellidos, id);
        this.bono2 = bono2;
        setTipo("Supervisor"); // Establece el tipo
        calcularSueldo();
    } //Fin de Constructor Supervisor con siete argumentos

    @Override
    // Metodo calcularSueldo de Supervisor
    public void calcularSueldo() {
        this.setSueldo((this.getCostoPorHoras() * this.getHorasTrabajadas()) + this.getBono() + this.bono2);
    } // Fin metodo calcularSueldo Supervisor

}

class Cocinero extends Empleado { //Clase

    public Cocinero() {//Constructor vacio
    }

    // Constructor de seis argumentos
    public Cocinero(double horasTrabajadas, double bono,
            double costoPorHora, String nombres, String apellidos, int id) {
        super(horasTrabajadas, bono, costoPorHora, nombres, apellidos, id);
        setTipo("Cocinero");
        this.calcularSueldo();
    } // Fin de Constructor
} // Fin clase cocinero

class Mesero extends Empleado {//Clase

    public double propinas;

    public Mesero() {//Constructor vacio
    }

    //Constructor de siete argumentos    
    public Mesero(double horasTrabajadas, double bono, double costoPorHora, String nombres, String apellidos, int id, double propinas) {
        super(horasTrabajadas, bono, costoPorHora, nombres, apellidos, id);
        this.propinas = propinas;
        setTipo("Mesero"); // Establece el tipo
        calcularSueldo();
    } // Fin constructor

    @Override
    // Metodo calcularSueldo para Mesero
    public void calcularSueldo() {//metodo
        this.setSueldo((this.getCostoPorHoras() * this.getHorasTrabajadas()) + this.getBono() + this.propinas);
    } // Fin metodo calcularSueldo
} // Fin de la clase Mesero

class Recepcionista extends Empleado {//Clase

    public double faltante;

    public Recepcionista() {//Constructor
    }

    //Constructor con siete argumentos
    public Recepcionista(double horasTrabajadas, double bono, double costoPorHora, String nombres, String apellidos, int id, double faltante) {
        super(horasTrabajadas, bono, costoPorHora, nombres, apellidos, id);
        this.faltante = faltante;
        setTipo("Recepcionista"); // Establece el tipo
        calcularSueldo();
    } //Fin de constructor Recepcionista de siete argumentos

    @Override
    // Metodo calcularSueldo Recepcionista
    public void calcularSueldo() {//metodo
        this.setSueldo((this.getCostoPorHoras() * this.getHorasTrabajadas()) + this.getBono() - this.faltante);
    } // FIn metodo calcularSueldo 
} // Fin de la clase Recepcionista
