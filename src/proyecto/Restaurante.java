package proyecto;

/**
 * @author leencarnacion
 * @author cavargas10
 */
import java.io.File;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;

public class Restaurante {
    //Inicializacion de variables
    Scanner leer = new Scanner(System.in);
    private ArrayList<Comida> comida = new ArrayList<Comida>();
    private ArrayList<Cliente> clientes = new ArrayList<Cliente>();
    private ArrayList<Double> compras = new ArrayList<Double>();
    private ArrayList<Integer> listaCompras = new ArrayList<>();
    public ArrayList<String> platoDesayuno = new ArrayList<>();
    public ArrayList<String> platoAlmuerzo = new ArrayList<>();
    public ArrayList<String> platoAlmuerzoSegundo = new ArrayList<>();
    public ArrayList<String> platoMerienda = new ArrayList<>();
    public ArrayList<Double> precioDesayuno = new ArrayList<>();
    public ArrayList<Double> precioAlmuerzo = new ArrayList<>();
    public ArrayList<Double> precioAlmuerzoSegundo = new ArrayList<>();
    public ArrayList<Double> precioMerienda = new ArrayList<>();
    public int cont = 0;
    public int cont2 = 0;
    //metodo getCliente
    public ArrayList<Cliente> getClientes() {
        return clientes;
    }
    public static void main(String[] args) {
        Restaurante restaurante = new Restaurante(); // Invoca al constructor Restaurante
        Scanner leer = new Scanner(System.in);
        boolean terminar = true;
        int id = 0;

        // Crea instancias de Objeto Empleado
        Empleado supervisor = new Supervisor(8, 5, 2.75, "Manuel", "Cueva", 2, 10);
        Empleado mesero1 = new Mesero(8, 2, 2, "Franco", "Castillo", 3, 5);
        Empleado recep = new Recepcionista(8, 2.50, 2.25, "Bryan", "Guaman", 1, 10);
        Empleado cocinero = new Cocinero(8, 2, 2.10, "Andres", "Calva", 4);
        boolean terminar2 = true;
        do {

            // Intenta ingresar Cedula con valores invalidos
            try {
                System.out.println("INGRESE ID DE PERSONAL O DIGITE [0] PARA "
                        + "SALIR");
                System.out.printf("Digite el Id o [0]:");
                id = leer.nextInt();
                leer.nextLine();
                if (recep.getId() == id) { // Verificacion de Id

                    do {
                        // Intenta ingresar otra Factura con valores invalidos
                        try {
                            System.out.println("[1]AGREGAR VENTA"
                                    + " o [0]SALIR");
                            System.out.printf("Digite la opcion [1] o [0]: ");
                            id = leer.nextInt();
                            leer.nextLine();
                            if (id == 0) {
                                terminar2 = false;
                            } else if (id == 1) {
                                restaurante.llenarArreglos();
                                String nombre;
                                String apellido;
                                String direccion;
                                int telefono = 0;
                                boolean terminar3 = true;
                                boolean terminar4 = true;
                                String tipoComida = "";
                                int tipoComidas = 0;
                                int temp = 0;
                                double valores = 0;
                                Cliente tempCliente;
                                Cliente cliente = null;
                                System.out.println("INGRESE DATOS DEL CLIENTE\n");
                                System.out.printf("INGRESE NOMBRE: ");
                                nombre = leer.nextLine();
                                System.out.printf("INGRESE APELLIDO: ");
                                apellido = leer.nextLine();
                                System.out.printf("INGRESE DIRECCION: ");
                                direccion = leer.nextLine();
                                do {
                                    // Intenta ingresar telefono con valores invalidos
                                    try {
                                        System.out.printf("INGRESE TELEFONO:");
                                        telefono = leer.nextInt();
                                        leer.nextLine();
                                        terminar3 = false;
                                    } // Fin de try para ingresar telefono
                                    catch (Exception e) {
                                        System.out.println("INGRESE NUMEROS");
                                        terminar3 = true;
                                        leer.nextLine();
                                    } // Fin de catch para ingresat telefono

                                } while (terminar3);

                                do {
                                    // Intenta ingresar cedula con valores invalidos
                                    try {
                                        System.out.printf("INGRESE CEDULA: ");
                                        id = leer.nextInt();
                                        leer.nextLine();
                                        terminar3 = false;
                                        for (int i = 0; i < restaurante.clientes.size(); i++) {
                                            tempCliente = restaurante.clientes.get(i);
                                            if (id == tempCliente.getId()) {
                                                System.out.println("LA CEDULA INGRESADA, YA SE ENCUENTRA REGISTRADA");
                                                terminar3 = true;
                                            }
                                        }

                                        if (!terminar3) {
                                            do {
                                                if (terminar4) {
                                                    cliente = new Cliente(telefono, direccion, nombre, apellido, id);
                                                    restaurante.clientes.add(cliente);
                                                    terminar4 = false;
                                                }
                                                if (!terminar4) {
                                                    // Intenta ingresar tipoComidas con valores invalidos
                                                    try {
                                                        System.out.println("MENU: \n[1]DESAYUNO\n[2]ALMUERZO\n[3]MERIENDA");
                                                        System.out.printf("Digite la opcion: ");
                                                        tipoComidas = leer.nextInt();
                                                        leer.nextLine();
                                                        // Verificacion tipoComidas
                                                        if (tipoComidas == 1 || tipoComidas == 2 || tipoComidas == 3) {
                                                            terminar3 = false;
                                                        } else {
                                                            System.out.println("INTENTE NUEVAMENTE");
                                                            terminar3 = true;
                                                        }
                                                    } // Fin de try para tipoComidas
                                                    catch (Exception e) {
                                                        leer.nextLine();
                                                        terminar3 = true;
                                                        System.out.println("USTED HA INGRESDO DATOS NO VALIDOS");
                                                    } // Fin de catch para tipoComidas
                                                }

                                            } while (terminar3);
                                            Comida tempComida;
                                            do {
                                                if (tipoComidas == 1 || tipoComidas == 2 || tipoComidas == 3) {
                                                    if (tipoComidas == 1) {
                                                        tipoComida = "Desayuno"; //Establece tipoComida
                                                    } else if (tipoComidas == 2) {
                                                        tipoComida = "Almuerzo"; //Establece tipoComida
                                                    } else if (tipoComidas == 3) {
                                                        tipoComida = "Merienda"; //Establece tipoComida
                                                    }
                                                    int n1, n2, cant = 0, temps;
                                                    boolean finalizar = true;
                                                    do {
                                                        // try para tipoComida Desayuno
                                                        try {
                                                            if (tipoComida == "Desayuno") {
                                                                for (int i = 0; i < restaurante.platoDesayuno.size(); i++) {
                                                                    System.out.println((i + 1) + ". " + restaurante.platoDesayuno.get(i));
                                                                }
                                                                n1 = leer.nextInt();
                                                                leer.nextLine();
                                                                do {
                                                                    // try para Cantidad Desayuno
                                                                    try {
                                                                        if (n1 > 0 && n1 < restaurante.platoDesayuno.size() + 1) {
                                                                            System.out.println("Cantidad:");
                                                                            cant = leer.nextInt();
                                                                            leer.nextLine();
                                                                            // Verificar Cantidad Desayuno
                                                                            if (cant > 0) {
                                                                                Comida desayuno = new Desayuno(restaurante.platoDesayuno.get(n1 - 1),restaurante.precioDesayuno.get(n1-1), cant);
                                                                                restaurante.comida.add(desayuno); // Agregar a ArrayList desayuno
                                                                                restaurante.platoDesayuno.remove(n1 - 1); // Remover desayuno agregado
                                                                                finalizar = false;
                                                                            } else {
                                                                                System.out.println("CANTIDAD INCORRECTA");
                                                                                finalizar = true;
                                                                            }

                                                                        } else {
                                                                            System.out.printf("INTENTE NUEVAMENTE ");
                                                                            finalizar = true;
                                                                            n1 = leer.nextInt();
                                                                            leer.nextLine();

                                                                        }
                                                                    } // Fin try para Cantidad Desayuno 
                                                                    catch (Exception e) {
                                                                        finalizar = true;
                                                                        leer.nextLine();
                                                                        System.out.println("DATOS INCORRECTOS");
                                                                    } // Fin catch

                                                                } while (finalizar);
                                                            } else if (tipoComida == "Almuerzo") {
                                                                System.out.println("Entrada");
                                                                for (int i = 0; i < restaurante.platoAlmuerzo.size(); i++) {
                                                                    System.out.println((i + 1) + ". " + restaurante.platoAlmuerzo.get(i));
                                                                }
                                                                n1 = leer.nextInt();
                                                                leer.nextLine();
                                                                do {
                                                                    //try de cantidad de comida
                                                                    try {

                                                                        if (n1 > 0 && n1 < restaurante.platoAlmuerzo.size() + 1) {
                                                                            System.out.printf("Ingrese Cantidad: ");
                                                                            cant = leer.nextInt();
                                                                            leer.nextLine();
                                                                            if (cant > 0) {
                                                                                finalizar = false;
                                                                            } else {
                                                                                System.out.printf("CANTIDAD INCORRECTA");
                                                                            }

                                                                        } else {

                                                                            System.out.println("INTENTE NUEVAMENTE");
                                                                            n1 = leer.nextInt();
                                                                            leer.nextLine();
                                                                        }
                                                                    } catch (Exception e) {
                                                                    
                                                                        finalizar = true;
                                                                        leer.nextLine();
                                                                        System.out.println("DATOS INCORRECTOS");
                                                                    }//Fin catch para cantidadd de comida

                                                                } while (finalizar);
                                                                System.out.println("Segundo");
                                                                for (int i = 0; i < restaurante.platoAlmuerzoSegundo.size(); i++) {
                                                                    System.out.println((i + 1) + ". " + restaurante.platoAlmuerzoSegundo.get(i));
                                                                }
                                                                n2 = leer.nextInt();
                                                                leer.nextLine();
                                                                do {
                                                                    //try para  agregar la compra
                                                                    try {
                                                                        if (n2 > 0 && n2 < restaurante.platoAlmuerzoSegundo.size() + 1) {
                                                                            Comida almuerzo = new Almuerzo(restaurante.platoAlmuerzo.get(n1 - 1), restaurante.precioAlmuerzo.get(n1-1), cant, restaurante.precioAlmuerzoSegundo.get(n1-1), restaurante.platoAlmuerzoSegundo.get(n2 - 1));
                                                                            restaurante.comida.add(almuerzo);
                                                                            restaurante.platoAlmuerzo.remove(n1 - 1);
                                                                            restaurante.platoAlmuerzoSegundo.remove(n2 - 1);
                                                                            finalizar = false;
                                                                        } else {
                                                                            finalizar = true;
                                                                            System.out.println("INTENTE NUEVAMENTE");
                                                                            n2 = leer.nextInt();
                                                                            leer.nextLine();
                                                                        }
                                                                    } catch (Exception e) {
                                                                        finalizar = true;
                                                                        leer.nextLine();
                                                                        System.out.println("DATOS INCORRECTOS");
                                                                    }//Fin catch agregar compra
                                                                } while (finalizar);

                                                            } else if (tipoComida == "Merienda") {
                                                                for (int i = 0; i < restaurante.platoMerienda.size(); i++) {
                                                                    System.out.println((i + 1) + ". " + restaurante.platoMerienda.get(i));
                                                                }
                                                                n1 = leer.nextInt();
                                                                leer.nextLine();
                                                                do {
                                                                    // Try cantidad de comida
                                                                    try {

                                                                        if (n1 > 0 && n1 < restaurante.platoMerienda.size() + 1) {

                                                                            System.out.println("Ingrese Cantidad: ");
                                                                            cant = leer.nextInt();
                                                                            leer.nextLine();
                                                                            if (cant > 0) {
                                                                                Comida merienda = new Merienda(restaurante.platoMerienda.get(n1 - 1), restaurante.precioMerienda.get(n1-1), cant);
                                                                                restaurante.comida.add(merienda);
                                                                                restaurante.platoMerienda.remove(n1 - 1);
                                                                                finalizar = false;
                                                                            } else {
                                                                                finalizar = true;
                                                                                System.out.println("CANTIDAD INCORRECTA");
                                                                            }

                                                                        } else {
                                                                            finalizar = true;
                                                                            System.out.println("INTENTE NUEVAMENTE");
                                                                            n1 = leer.nextInt();
                                                                            leer.nextLine();
                                                                        }
                                                                    } catch (Exception e) {
                                                                        leer.nextLine();
                                                                        finalizar = true;
                                                                        System.out.println("DATOS INCORRECTOS");
                                                                    }//catch agregar comida

                                                                } while (finalizar);

                                                            }
                                                        } // Fin try para tipoComida Desayuno
                                                        catch (Exception e) {
                                                            leer.nextLine();
                                                            finalizar = true;
                                                            System.out.printf("INGRESE NUMERO POR FAVOR ");
                                                        } // Fin catch
                                                    } while (finalizar);

                                                    restaurante.cont++;
                                                    terminar3 = false;
                                                    tempComida = restaurante.comida.get(restaurante.comida.size() - 1);
                                                    valores += tempComida.getTotal();
                                                    // Verificacion platos Desayuno
                                                    if (restaurante.platoDesayuno.size() == 0 || restaurante.platoAlmuerzo.size() == 0 || restaurante.platoMerienda.size() == 0) {
                                                        System.out.println("NO HAY MAS PLATOS DISPONIBLES");
                                                        terminar3 = false;
                                                    } else {
                                                        do {
                                                            // Intenta ingresar otro plato con valores invalidos
                                                            try {
                                                                System.out.println("[1] AGREGAR OTRO PLATO"
                                                                        + "O [0] SALIR");
                                                                System.out.printf("Digite la opcion: ");
                                                                temp = leer.nextInt();
                                                                leer.nextLine();
                                                                // Verificacion agregar otro plato
                                                                if (temp == 1 || temp == 0) {
                                                                    terminar3 = false;
                                                                } else {
                                                                    terminar3 = true;
                                                                    System.out.println("OPCION NO DISPONIBLE");
                                                                }

                                                            }// Fin de try para ingresar otro plato 
                                                            catch (Exception e) {
                                                                leer.nextLine();
                                                                terminar3 = true;
                                                                System.out.println("USTED HA INGRESADO DATOS NO VALIDOS");
                                                            }// Fin de catch para ingresar otro plato

                                                        } while (terminar3);
                                                        if (temp == 0) {
                                                            terminar3 = false;
                                                        } else if (temp == 1) {
                                                            terminar3 = true;
                                                        }

                                                    }
                                                } else {
                                                    System.out.println("Opcion no disponible");
                                                    tipoComidas = leer.nextInt();
                                                    leer.nextLine();
                                                }

                                            } while (terminar3);

                                        }

                                    }// Fin de try para ingresar cedula 
                                    catch (Exception e) {
                                        leer.nextLine();
                                        terminar3 = true;
                                        System.out.println("USTED HA INGRESADO DATOS NO VALIDOS");
                                    } // Fin de catch para ingresar cedula
                                } while (terminar3);
                                restaurante.cont2++;

                                restaurante.listaCompras.add(restaurante.cont);
                                restaurante.compras.add(valores);
                                System.out.println("---- VENTA FINALIZADA ----");
                                System.out.println("=== FACTURA ===");
                                System.out.println(cliente);
                                System.out.println("Tipo de comida:" + tipoComida);
                                System.out.println(restaurante.mostrarFactura());
                                System.out.println("Total Final: " + valores);
                                try {
                                    String resumen = "";
                                    //Lectura linea a linea
                                    //FLUJO DE SALIDA del archivo
                                    Formatter Factura = new Formatter("Factura" + restaurante.cont2 + ".txt");
                                    resumen+="--------------------------------------Factura--------------------------------------%n"
                                            + cliente+"\n"
                                            + "Tipo de comida:"+tipoComida+"%n"
                                            +restaurante.mostrarFactura()
                                            +"Total Final: "+valores;
                                    Factura.format(resumen);
                                    Factura.close();
                                    System.out.println("Se ha generado Factura con satisfación");
                                } catch (Exception e) {
                                    terminar2 = true;
                                }
                            } // Fin de try para ingresar otra Factura
                        } catch (Exception e) {
                            leer.nextLine();
                            System.out.printf("INGRESE UN NUMERO POR FAVOR: ");
                            terminar2 = true;
                        } // Fin de catch para ingresar otra Factura
                    } while (terminar2);

                } else if (supervisor.getId() == id) {

                    ArrayList<Cliente> cliente = restaurante.getClientes();
                    if (cliente.size() == 0) {
                        System.out.println("NO HAY VENTAS REALIZADAS EL DIA DE HOY");
                    } else {
                        do {
                            restaurante.mostrarGanancias(mesero1, recep, cocinero, supervisor);
                        } while (terminar2);
                    }

                } else if (id == 0) {
                    terminar = false;
                } else {
                    System.out.println("IDENTIFICACION EQUIVOCADA");
                }
            } // Fin de try para ingresar Cedula
            catch (Exception e) {
                leer.nextLine();
                System.out.println("USTED HA INGRESADO DATOS INVALIDOS");
                terminar = true;
            } // Fin de catch para ingresar Cedula

        } while (terminar);
    }

    public String mostrarFactura() {
        String temp="";
        int n1 = listaCompras.get(listaCompras.size() - 1);
        int n2 = 0;
        int n3 = listaCompras.size();
        if (n3 == 1) {
            n2 = 0;
        } else {
            n2 = listaCompras.get(listaCompras.size() - 2);
        }
        for (int i = n1; i > n2; i--) {
            temp+=(comida.get(i - 1));
        }
        return temp;
    }

    public void mostrarGanancias(Empleado mesero, Empleado recepcionista, Empleado cocinero, Empleado supervisor) {
        double totalventas = 0;
        double totalgastos = 0;
        String resumen = "Resumen del dia%n"
                + "--------------------------------------Gastos--------------------------------------%n"
                + "Empleados:%n"
                + supervisor + "%n"
                + mesero + "%n"
                + recepcionista + "%n"
                + cocinero + "%n"
                + "--------------------------------------Ventas--------------------------------------%n";
        totalgastos += supervisor.getSueldo();
        totalgastos += mesero.getSueldo();
        totalgastos += recepcionista.getSueldo();
        totalgastos += cocinero.getSueldo();
        for (int i = 0; i < clientes.size(); i++) {
            resumen += clientes.get(i);
            resumen += "%nGastos del cliente : " + compras.get(i) + "%n";
            totalventas += compras.get(i);
        }
        resumen += "--------------------------------------Resumen--------------------------------------%n"
                + "Ganacia en ventas: " + totalventas + "\tGastos empleados: " + totalgastos + "%n"
                + "+GANANCIA / -PERDIDA: " + (totalventas - totalgastos);
        try {
            //FLUJO DE SALIDA del archivo
            Formatter GanaciaPerdida = new Formatter("GananciaPerdida.txt");
            GanaciaPerdida.format(resumen);
            GanaciaPerdida.close();
            System.out.println("Se han creado los balances del dia con sastifacción\n");
        } catch (Exception e) {
        }

    }

    public void llenarArreglos() {
        platoDesayuno.clear();
        platoAlmuerzo.clear();
        platoAlmuerzoSegundo.clear();
        platoMerienda.clear();
        precioDesayuno.clear();
        precioAlmuerzo.clear();
        precioAlmuerzoSegundo.clear();
        precioMerienda.clear();
        try {
            //FLUJO DE ENTRADA de varios archivos
            Scanner desayuno = new Scanner(new File("desayuno.csv"));
            Scanner almuerzo = new Scanner(new File("almuerzo.csv"));
            Scanner segundoalmuerzo = new Scanner(new File("segundoalmuerzo.csv"));
            Scanner merienda = new Scanner(new File("merienda.csv"));
            String temp1;
            //Lectura linea a linea
            while (desayuno.hasNext()) {
                temp1 = desayuno.nextLine();
                String []arreglo=temp1.split(";");
                platoDesayuno.add(arreglo[0]);
                precioDesayuno.add(Double.parseDouble(arreglo[1]));
            }
            //Lectura linea a linea
            while (almuerzo.hasNext()) {
                temp1 = almuerzo.nextLine();
                String [] arreglo=temp1.split(";");
                platoAlmuerzo.add(arreglo[0]);
                precioAlmuerzo.add(Double.parseDouble(arreglo[1]));
            }
            //Lectura linea a linea
            while (segundoalmuerzo.hasNext()) {
                temp1 = segundoalmuerzo.nextLine();
                String [] arreglo=temp1.split(";");
                platoAlmuerzoSegundo.add(arreglo[0]);
                precioAlmuerzoSegundo.add(Double.parseDouble(arreglo[1]));
            }
            //Lectura linea a linea
            while (merienda.hasNext()) {
                temp1 = merienda.nextLine();
                String []arreglo=temp1.split(";");
                platoMerienda.add(arreglo[0]);
                precioMerienda.add(Double.parseDouble(arreglo[1]));
            }
            desayuno.close();
            almuerzo.close();
            segundoalmuerzo.close();
            merienda.close();
        } catch (Exception e) {
        }
    }

}